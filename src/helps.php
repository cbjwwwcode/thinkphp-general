<?php

/**
 * 公共函数
 */

use CbjCode\TpGeneral\Common\Random;


if (!function_exists('check_image_domain')) {
    /**
     * 图片资源路径域名补全函数
     * @param string|array $image 图片路径
     * @param bool $type 类型:false=string|true=array
     * @param string $rule 分割规则
     * @param string $http 协议
     * @return array|mixed|string
     */
    function check_image_domain($image = "", bool $type = false, string $rule = ",", string $http="http")
    {

        if ($image) {
            if ($type){
                if (is_array($image)){
                    foreach ($image as $k=>$v){
                        $image[$k] = check_image_domain($v,false,$rule,$http);
                    }
                }elseif (is_string($image)){
                    $rule = $rule?$rule:",";
                    $image = check_image_domain(explode($rule,$image),$type,$rule,$http);
                }
            }else{
                if (strpos($image, $http) === false) {
                    $image =  function_exists('cdnurl') ? cdnurl($image,true) :request()->domain(). $image;
                }
            }
        }
        return $image;
    }
}

if(!function_exists('numToChinese')) {
    /**
     * 数字转换为中文
     * @param string|integer|float $num 目标数字
     * @param bool $mode 模式[true:金额（默认）,false:普通数字表示]
     * @param bool $sim 使用小写（默认）
     * @return string
     */
    function numToChinese($num = '', bool $mode = false,bool $sim = true): string
    {
        if(!is_numeric($num)) return '含有非数字非小数点字符！';
        if ($num<0||$num>=pow(10,16)) return '请输入大于0且小于10的16次方的数字';
        $char = $sim ? array('零','一','二','三','四','五','六','七','八','九') : array('零','壹','贰','叁','肆','伍','陆','柒','捌','玖');
        $unit = $sim ? array('','十','百','千','','万','亿','兆') : array('','拾','佰','仟','','萬','億','兆');
        $return = $mode ? '元':'点';
        //小数部分
        if(strpos($num, '.')){
            list($num,$dec) = explode('.', $num);
            $dec = strval(round($dec,2));
            if($mode){
                $return .= "{$char[$dec[0]]}角{$char[$dec[1]]}分";
            }else{
                for($i = 0,$c = strlen($dec);$i < $c;$i++) {
                    $return .= $char[$dec[$i]];
                }
            }
        }
        $out = array();
        //整数部分
        $str = $mode ? strrev(intval($num)) : strrev($num);
        for($i = 0,$c = strlen($str);$i < $c;$i++) {
            $out[$i] = $char[$str[$i]];
            if($mode){
                $out[$i] .= $str[$i] != '0'? $unit[$i%4] : '';
                if($i>1 and $str[$i]+$str[$i-1] == 0){
                    $out[$i] = '';
                }
                if($i%4 == 0){
                    $out[$i] .= $unit[4+floor($i/4)];
                }
            }
        }
        $return = join('',array_reverse($out)) . $return;
        return $return;
    }
}

if (!function_exists('getDistance')) {
    /**
     * 计算两点之间的距离
     * @param float $lng1 经度1
     * @param float $lat1 纬度1
     * @param float $lng2 经度2
     * @param float $lat2 纬度2
     * @param int $unit m，km
     * @param int $decimal 位数
     * @return float
     */
    function getDistance(float $lng1, float $lat1, float $lng2, float $lat2, int $unit = 2, int $decimal = 2)
    {
        $EARTH_RADIUS = 6370.996; // 地球半径系数
        $PI           = 3.1415926535898;
        $radLat1 = $lat1 * $PI / 180.0;
        $radLat2 = $lat2 * $PI / 180.0;
        $radLng1 = $lng1 * $PI / 180.0;
        $radLng2 = $lng2 * $PI / 180.0;
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2)));
        $distance = $distance * $EARTH_RADIUS * 1000;
        if ($unit === 2) {
            $distance /= 1000;
        }
        return round($distance, $decimal);
    }

}

if (!function_exists('replacePicUrl')) {
    /**
     * 替换富文本中的图片 添加域名
     * @param string $content 要替换的内容
     * @param string $domain 域名地址
     * @return string
     * @eg
     */
    function replacePicUrl(string $content = null,string $domain = '') {
        $path = '/uploads';
        $url = $domain?$domain.$path:(function_exists('cdnurl') ? cdnurl($path,true) :request()->domain(). $path);
        $pregRule = "/<[img|IMG].*?src=[\'|\"]\/uploads(.*?(?:[\.jpg|\.jpeg|\.png|\.gif|\.bmp]))[\'|\"]/";
        $content = preg_replace($pregRule, '<img src="'.$url.'${1}" style="max-width:100%;display: inline-block;height:auto"', $content);
        $content = preg_replace("/&nbsp;/", '<span style="width:3px;display: inline-block"></span>', $content);
        $content = preg_replace("/\n/", '<br />', $content);
        $content = preg_replace("/\r/", '<br />', $content);
        $content = str_replace('text-indent:32.0pt;','',$content);
        $content = str_replace('mso-char-indent-count:2.0;','',$content);
        $content = str_replace('line-height:28.0pt;','',$content);
        $content = str_replace('mso-line-height-rule:exactly','',$content);
        $content = str_replace('text-indent:32.0pt','',$content);
        $content = str_replace('mso-char-indent-count:2.0','',$content);
        $content = str_replace('line-height:28.0pt','',$content);
        return $content;
    }
}


if (!function_exists('build_order_no')) {
    /**
     * 生成长订单号
     * @param int $number
     * @return string
     */
    function build_order_no(int $number = 6): string
    {
        @date_default_timezone_set("PRC");
        $order_id_main = date('YmdHis') . Random::numeric($number);
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for($i=0; $i<$order_id_len; $i++){
            $order_id_sum += (int)(substr($order_id_main,$i,1));
        }
        return $order_id_main . str_pad((100 - $order_id_sum % 100) % 100,2,'0',STR_PAD_LEFT);
    }
}

if (!function_exists('orderNo')){
    /**
     * 生成短订单号
     * @return string
     */
    function orderNo(){
        return date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
    }
}

if (!function_exists('modelError')){
    /**
     * 错误提示
     * @param string $msg
     * @return void
     * @throws \think\Exception
     */
    function modelError(string $msg = ''){
        throw new \think\Exception($msg);
    }
}


if (!function_exists('jsonError')){
    /**
     * 错误提示
     * @param string $msg
     * @param mixed $data
     * @return array
     */
    function jsonError(string $msg = 'ok', $data = []): array
    {
        return jsonResult(0,$msg,$data);
    }
}

if (!function_exists('jsonSuccess')){
    /**
     * 成功提示
     * @param string $msg
     * @param mixed $data
     * @return array
     */
    function jsonSuccess(string $msg = 'ok', $data = []): array
    {
        return jsonResult(1,$msg,$data);
    }
}

if (!function_exists('jsonResult')){
    /**
     * 成功提示
     * @param int $code
     * @param string $msg
     * @param mixed $data
     * @return array
     */
    function jsonResult(int $code, string $msg = '', $data = []): array
    {
        return ['code'=>$code,'msg'=>$msg,'data'=>$data];
    }
}