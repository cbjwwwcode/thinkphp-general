<?php

namespace CbjCode\TpGeneral\Library\AliYum;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsResponse;
use Darabonba\OpenApi\Models\Config;
use think\Env;

class AliYum
{
    protected $endpoint = "dysmsapi.aliyuncs.com";

    static $client = null;

    public function __construct($accessKeyId = '',$accessKeySecret = ''){
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => Env::get('aliYumSms.AccessKey_ID',$accessKeyId),
            // 您的AccessKey Secret
            "accessKeySecret" => Env::get('aliYumSms.AccessKey_Secret',$accessKeySecret),
        ]);
        // 访问的域名
        $config->endpoint = $this->endpoint;
        self::$client = new Dysmsapi($config);
    }

    /**
     * @param string|int $phone 电话
     * @param string $sign 签名
     * @param string $code 短信模板ID
     * @param string|array $param 短信模板变量对应的实际值
     * @return SendSmsResponse
     */
    public static function send($phone, string $sign = '', string $code = '', $param = ''): SendSmsResponse
    {
        $sendSmsRequest = new SendSmsRequest([
            "phoneNumbers" => $phone,
            "signName" =>  $sign,
            "templateCode" =>  $code,
            "templateParam" =>  is_array($param)?json_encode($param,JSON_UNESCAPED_UNICODE):$param
        ]);
        return self::$client->sendSms($sendSmsRequest);
    }
}
